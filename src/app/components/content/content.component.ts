import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';

import { Product } from '../../models/product';


@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  isPalindromo: boolean;
  isError: boolean;
  showError: string;
  data: Product[] ;
  empty: string;

  constructor(private apiService: ApiService) { }

  ngOnInit() { 
    this.isError = false;
    this.isPalindromo = false;
    this.empty = 'Realice una búsqueda';

  }

  searchProducts(product: string) {  

      this.apiService.getProducts(product).then(productsResponse => {
        this.data = productsResponse.data; 
        this.isError=false;  
        this.isPalindromo = productsResponse.palindrome;
        if(this.data.length==0){
          this.empty = 'Busqueda sin resultado';
        }
      }).catch(err => {this.showError = err.error.mensaje; this.isError = true; this.data = [];this.isPalindromo = false;});    
  }

}