import { ProductsList } from './productsList';
export interface ProductsResponse {
  palindrome:boolean;
  data:ProductsList;
}
