import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { ProductsResponse } from 'src/app/models/product.response';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  public getProducts(data){
    
      return this.httpClient.get<ProductsResponse>(environment.urlBackend.product + data)
    .toPromise()
    .catch(err => {throw err})
    .then(res => res)
    .then(data => {return data});
    
  }
}
